var fs = require('fs');
var loopback = require('loopback');
var app_dir = '../common/models/';
var app = require('./server');
var dataSource = app.dataSources.vps;
var modelconfiglocal = 'model-config.local.json';
var modelstruct = 'modelname.js';
//var db = 'postgresql', owner = 'svsycxrezdbohl';
var newModels = '{';

function GenerateFiles(model_name, lista){ 
    for(var j in lista){ 
        if(lista[j]==model_name){ 
            return true; 
        } 
    } 
    return false;     
}

function SaveJs(string) {
    fs.readFile(modelstruct, 'utf8', function (err, data) {
        if (err) {
            return console.log(err);
        }
        var result = data.replace(/MODELNAME/g, string);

        fs.writeFile(app_dir + capitaliseFirstLetter(string) + '.js', result, 'utf8', function (err) {
            if (err) return console.log(err);
            console.log('Saved ' + app_dir + capitaliseFirstLetter(string) + '.js');
        });
    });
};

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + LowerCase(string.slice(1));
}

function LowerCase(string) {
    return string.toLowerCase();
}

function jsFileString(model_name) {
    return '' +
        'module.exports = function(' + capitaliseFirstLetter(model_name) + ') {\n' +
        '\t\n' +
        '};';
}

function autoGenerateModelFiles() {
    dataSource.discoverModelDefinitions({ views: false, limit: 50 }, function (err, models) {
        models.forEach(function (model) {
            dataSource.discoverSchema(model.name, { associations: true }, function (err, schema) {
                newModels = newModels + '"' + capitaliseFirstLetter(model.name) + '": {"dataSource": "vps", "public": true},\n';
                try {
                    schema.options.plural = model.name;
                } catch (error) {
                    console.log('Error settings plurals on ' + app_dir + capitaliseFirstLetter(model.name));
                }
                ModelosStandar=['acl','accesstoken','role','rolemapping','user']; 
                if (GenerateFiles(model.name)==false){
                if (!fs.existsSync(app_dir + capitaliseFirstLetter(model.name) + '.json')) {


                    fs.writeFile(app_dir + capitaliseFirstLetter(model.name) + '.json', JSON.stringify(schema, null, '  '), function (err) {
                        if (err) throw err;
                        console.log('Saved ' + app_dir + capitaliseFirstLetter(model.name));
                    });
                    /* fs.writeFile(app_dir + capitaliseFirstLetter(model.name) + '.js', jsFileString(model.name), function (err) {
                         if (err) throw err;
                         console.log('Created ' + app_dir + capitaliseFirstLetter(model.name) + '.json file');
                     });*/
                }
                if (!fs.existsSync(app_dir + capitaliseFirstLetter(model.name) + '.js')) {
                    SaveJs(model.name);
                }
                fs.writeFile(modelconfiglocal, newModels + '}', function (err) {
                    if (err) throw err; console.log('Saved ' + modelconfiglocal);
                });
                };
            });
        });
    });
}

autoGenerateModelFiles();
