(function () {
    angular.module('SPApp.directives', [])
        .directive('clienteRow', function () {
            return {
                restrict: 'E',
                templateUrl: 'views/partials/clienteRow.html'
            };
        })

})();
