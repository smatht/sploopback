(function () {
    angular.module('SPApp.services', [])
        // Servicios para LOCALSTORAGE de productos
        .factory('localStorageService', ['$http', '$q', '$window', function ($http, $q, $window) {
            var localStorage = $window.localStorage;

            function getProducts() {
                var products = localStorage.getItem('compra');

                if (!products) 
                    products = [];
                else 
                    products = JSON.parse(products);
        
                return products;
            }

            function getCantidad(idproducto) {
                var lsProductos = getProducts();
                var cant = 0;
                for (let i = 0; i < lsProductos.length; ++i) {
                    if(lsProductos[i].idproducto == idproducto)
                      cant = lsProductos[i].cantidad;
                }
                return cant;
            }

            function setCantidad(idproducto, cantidad) {
                var lsProductos = getProducts();
                var cant = 0;
                for (let i = 0; i < lsProductos.length; ++i) {
                    if(lsProductos[i].idproducto == idproducto){
                        lsProductos[i].cantidad = cantidad;
                    }
                }
                localStorage.setItem('compra', JSON.stringify(lsProductos));
            }

            function saveProduct(id) {
                var lsProductos = getProducts();
                let productsReduce = [];
                let exist = 0;
                for (let i = 0; i < lsProductos.length; ++i) {
                        if(lsProductos[i].idproducto == id)
                          exist = 1;
                }
                if(exist == 0){
                    var cantidad = prompt("Ingrese cantidad:", "1");
                    if (isNaN(cantidad) || cantidad == null || cantidad == "")
                        cantidad = "1";
                    lsProductos.push({
                        "idproducto": id,
                        "cantidad": cantidad
                    });
                    productsReduce = lsProductos;
                }else {
                  for (let j = 0; j < lsProductos.length; ++j) {
                        if(lsProductos[j].idproducto != id)
                          productsReduce.push(lsProductos[j]);
                   }
                   lsDeleteProduct(id, lsProductos)
                }
                localStorage.setItem('compra', JSON.stringify(productsReduce));
            }

            function lsDeleteProduct(id, products = []) {
                if (products.length <= 0)
                    products = getProducts();
                let productsReduce = [];
                for (let j = 0; j < products.length; ++j){
                    if(products[j].idproducto != id)
                        productsReduce.push(products[j]);
                }
                localStorage.setItem("compra", JSON.stringify(productsReduce));
            }

            function lsDeleteAll() {
                localStorage.setItem("compra", JSON.stringify([]));
            }
        
            return {
                getAll: getProducts,
                save: saveProduct,
                delete: lsDeleteProduct,
                deleteAll: lsDeleteAll, 
                getCantidad: getCantidad,
                setCantidad: setCantidad
            };
        
        }])
        // Servicios para objetos producto
        .factory('productosApiService', ['$http', '$q', 'Productos', 'Itemslista', function ($http, $q, Productos, Itemslista) {

            function getProducts() {
                return prod = Productos.find(
                //     {
                //     filter: {
                //         limit:500
                //     }
                //   }
                );
                  
            }

            function getProductsFiltered(filter) {
                var deferred = $q.defer();
                Productos.find(filter, function(prod, err) {
                    deferred.resolve(prod);
                });
                return deferred.promise;
            }

            function getPrice(filter) {
                var deferred = $q.defer();
                Itemslista.find(filter, function(price, err) {
                    deferred.resolve(price);
                });
                return deferred.promise;
            }

            return {
                getAll: getProducts,
                getFiltered: getProductsFiltered,
                getPrice: getPrice
            };
        }])

        .factory('pedidoService', ['$http', '$q', 'Pedidos', 'Pedidositems', function ($http, $q, Pedidos, Pedidositems) {
            function getUltimo() {
                // return idPed = Productos.findOne(
                //         {
                //         filter: {
                //             order:"idpedido DESC"
                //         }
                //       }
                //     );
                var deferred = $q.defer();
                Pedidos.count(function (count){
                    deferred.resolve(count);
                });
                return deferred.promise;
            }

            function addPedido(nroEmpresa, nroPedido, nroVendedor, nroCliente, condVenta, observ, fpedido, fentrega, totalNeto, totalImpuesto, totalPedido, moneda, totDescuento, condPago, rDescuento, tipoPeido){

                return Pedidos
                            .create({
                                idpedido: nroPedido,
                                idvendedor: nroVendedor,
                                idcliente: nroCliente,
                                sobservaciones: observ,
                                fpedido: fpedido,
                                idcondventa: condVenta,
                                idcondpago: condPago,
                                idmoneda: moneda,
                                rtotdescuentos: totDescuento,
                                totalpedido: totalPedido,
                                totalimpuesto: totalImpuesto,
                                totalneto: totalNeto,
                                itipopedido: tipoPeido,
                                idempresa: nroEmpresa,
                                rdescuento: rDescuento
                            })
                            .$promise;
            }

            function addItems(nroEmpresa, nroPedido, arrayItems) {
                var ret = true;
                var data;
                for (var i = 0; i < arrayItems.length; i++) {
                    data = {
                        idempresa: nroEmpresa,
                        idpedido: nroPedido,
                        ncantidad: parseFloat(arrayItems[i].cantidad),
                        fprecio: parseFloat(arrayItems[i].precio),
                        iddeposito: 1,
                        idlote: arrayItems[i].idproducto,
                        idlista: 1,
                        idproducto: arrayItems[i].idproducto,
                        idunidad: 1,
                        rsubdescuento: 0,
                        icantumxunidad: 1,
                        rprecioorig: parseFloat(arrayItems[i].precio),
                        ccostouni: parseFloat(arrayItems[i].ccosto)
                    };
                    Pedidositems
                    .create(data, function(obj){
                        if (!obj)
                            ret = false;
                    });
                    
                    return ret;
                }

                // return Pedidos
                //     .create({
                //         idpedido: nroPedido,
                //         idvendedor: nroVendedor,
                //         idcliente: nroCliente,
                //         sobservaciones: observ,
                //         fpedido: fpedido,
                //         idcondventa: condVenta,
                //         idcondpago: condPago,
                //         idmoneda: moneda,
                //         rtotdescuentos: totDescuento,
                //         totalpedido: totalPedido,
                //         totalimpuesto: totalImpuesto,
                //         totalneto: totalNeto,
                //         itipopedido: tipoPeido,
                //         idempresa: nroEmpresa,
                //         rdescuento: rDescuento
                //     })
            }

            return {
                getUltimoId: getUltimo,
                altaPedido: addPedido,
                altaItems: addItems
            };

        }]);

})();