var app = angular.module('SPApp',[
	'ngRoute',
	'SPApp.clienteController',
	'SPApp.productoController',
    'SPApp.pedidoController',
	'SPApp.filters',
	'SPApp.services',
	'angularUtils.directives.dirPagination',
	'lbServices'
]);

// app.config(function(paginationTemplateProvider) {
//     paginationTemplateProvider.setPath('dirPagination.tpl.html');
// });

app.directive('clienteRow', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/clienteRow.html'
    }
});

app.config(['$routeProvider', function ($routeProvider) {
	$routeProvider
	.when('/', {
			templateUrl: 'views/home.html',
		})
		.when('/clientes/', {
			templateUrl: 'views/clientes.html',
			controller: 'clienteController',
		})
		.when('/productos/', {
			templateUrl: 'views/productos.html',
			controller: 'productoController',
		})
		.when('/pedido/', {
			templateUrl: 'views/pedido.html',
            controller: 'pedidoController'
		});
}]);
