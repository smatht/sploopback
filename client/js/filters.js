(function (){
    angular.module('SPApp.filters', [])

        .filter('getCantidad', ['$window', function ($window) {
            var cant = 0;
            var localStorage = $window.localStorage;
            var ls = [];
            ls = localStorage.getItem("compra")
            if (!ls){
                ls = [];
            } else {
                ls = JSON.parse(ls)
            }
            return function (input) {
                if (!input) return "";
                for (var i=0; i < ls.length; i++){
                    if (ls[i].idproducto == input.idproducto){
                        cant = ls[i].cantidad;
                        break;
                    }
                }
                return cant;
            };
        }])

        .filter('cantidad', ['$filter', function ($filter) {
            return function (input) {
                        var cant = $filter('getCantidad')(input);
                        return cant;
            };
        }]);
})();
