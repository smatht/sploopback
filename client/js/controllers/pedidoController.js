(function () {
    angular.module('SPApp.pedidoController', [])
        .controller('pedidoController', ['$scope', '$filter', 'localStorageService', 'productosApiService', 'pedidoService', 'Productos', function ($scope, $filter, localStorageService, productosApiService, pedidoService, Productos) {
            
            //////////////////////////////////////
            // Variables para cierre de pedido //
            //////////////////////////////////////
            var nroEmpresa = 1;
            var nroPedido = 0;
            var nroVendedor = 1;
            var nroCliente = 1;
            var condVenta = 'CDO';
            var observ = '';
            var fpedido = new Date();
            var fentrega = '';
            var totalNeto = 0;
            var totalImpuesto = 0;
            var totalPedido = 0; // Neto - descuento
            var moneda = 2;
            var totDescuento = 0;
            var condPago = 'EFE'; // Efectivo
            var rDescuento = 0;
            var tipoPeido = 0;
            /////////////////*/*/*////////////////////////

            $scope.inputDisable = true;
            $scope.productos = [];
            $scope.sumaTotal = 0;

            $scope.habilitarEdicion = function(){
                $scope.inputDisable = false;
            }
            
            $scope.c = function(id, cantidad){
                recalculoTotal();
                localStorageService.setCantidad(id,cantidad)
            };

            $scope.lsDeleteProduct = function(id){
                localStorageService.delete(id);
            };

            $scope.lsDeleteAllProducts = function(){
                localStorageService.deleteAll();
            };

            function recalculoTotal(){
                $scope.sumaTotal = 0;
                for (var i = 0; i < $scope.productos.length; i++){
                    $scope.sumaTotal += ($scope.productos[i].cantidad * $scope.productos[i].precio);
                }
            }

            $scope.viewDeleteProduct = function(i){
                $scope.productos.splice(i, 1);
                recalculoTotal();
            };

            $scope.viewDeleteAllProducts = function(){
                $scope.productos = [];
                recalculoTotal();
            };

            $scope.updateSubtotal = function(producto){
                console.log(producto);
            };

            $scope.getProducts = function(){
                if (consultaLocalStorage.length >= 1) {
                var filter = {"filter": {"where":{"or": consultaLocalStorage}}};
                    productosApiService.getFiltered(filter).then(function (data){// Trae los objetos de los productos seleccionados de la api
                        $scope.productos = data; 
                        var filterPrecio = {"filter": (
                            {"where":{"or": $scope.lsProductos}, order: "fvigencia DESC"}
                        )};
                        productosApiService.getPrice(filterPrecio).then(function (data){ // De los mismos productos traigo los precios
                            for (var i = 0; i < $scope.productos.length; i++){ // Bucles para agregar precio y cantidad al objeto producto
                                for (var j = 0; j < data.length; j++){
                                    if (data[j].idproducto == $scope.productos[i].idproducto){
                                        $scope.productos[i].precio = data[j].rprecio; // Inserta un nuevo atributo al objeto preducto, precio
                                        // let cant = $filter('cantidad')($scope.productos[i]); 
                                        // Obtiene la cantidad seleccionada por el usuario mediante el filtro cantidad
                                        let cant =localStorageService.getCantidad($scope.productos[i].idproducto);
                                        $scope.productos[i].cantidad = cant; // Inserta atributo cantidad al objeto poducto
                                        $scope.sumaTotal += (cant * data[j].rprecio) // Realiza la suma del total de la compra
                                        break;
                                    }
                                }
                            }
                            
                        });
                        
                    });
                
                }
            };

            $scope.cerrarPedido = function(){
                fpedido = new Date();
                totalNeto = $scope.sumaTotal;
                totalPedido = totalNeto - totDescuento;
                pedidoService.getUltimoId().then(function (data){
                    nroPedido = data.count + 1;
                    console.log(data.count);
                    console.log(nroPedido);
                    pedidoService.altaPedido(nroEmpresa, nroPedido, nroVendedor, nroCliente, condVenta, observ, fpedido, fentrega, totalNeto, totalImpuesto, totalPedido, moneda, totDescuento, condPago, rDescuento, tipoPeido).then(function (obj) {
                        if (obj){
                            var flag = pedidoService.altaItems(nroEmpresa, nroPedido, $scope.productos);
                            if (flag){
                                $scope.lsDeleteAllProducts();
                                $scope.viewDeleteAllProducts();
                            }
                        }
                    });
                });
            };

            var consultaLocalStorage = localStorageService.getAll();
            $scope.getProducts();
    }]);
})();