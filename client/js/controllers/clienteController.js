(function () {

	angular.module('SPApp.clienteController', [])
.controller('clienteController', function($scope, $http, Clientes) {
 
 	$scope.clientes = Clientes.find();
  	$scope.loading=true;
 
  	$scope.add = function(){
  		$scope.loading=true;
  		
  		//Clientes.create({title: $scope.todo.title,isDone:false }).$promise
 		//	 .then(function(todo) { 
 		//	 		$scope.todos.push(todo);
 		//	 		$scope.todo.title='';
 		//	 		$scope.loading=false;
 		//	  });;
  	};
 
  	$scope.delete = function($index){
  		
  		$scope.loading=true;
  		var cliente = $scope.clientes[$index];
  		
  		Clientes.destroyById({ id: cliente.id}).$promise
  		    .then(function() {
				$scope.clientes.splice($index,1);
				$scope.loading=false;
		     },function(msg){
                         alert(msg);
                     });
  	};
 
  	$scope.update = function(cliente){
  		cliente.$save();
  	};
	
});

})();