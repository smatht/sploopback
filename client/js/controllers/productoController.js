(function () {

	angular.module('SPApp.productoController', [])
        .controller('productoController', function($scope, $filter, localStorageService, productosApiService, Productos) {  
            //////////////////////////////////////////////////////////////
            /// VARIABLES
            /////////////////////////////////////////////////////////////          
            $scope.productos = [];
            $scope.loading=false;
            $scope.productosSeleccionados = [];
            $scope.precios = [];
            $scope.sumaTotalSeleccionado = 0
            // Para paginacion
            $scope.filteredTodos = [];
            $scope.currentPage = 1;
            $scope.numPerPage = 10;
            $scope.maxSize = 5;

            //////////////////////////////////////////////////////////////
            /// FIN VARIABLES
            ///////////////////////////////////////////////////////////// 


            //////////////////////////////////////////////////////////////
            /// FUNCIONES Y METODOS
            ///////////////////////////////////////////////////////////// 

            // $scope.delete = function($index){
            //     $scope.loading=true;
            //     var producto = $scope.productos[$index];

            //     Productos.deleteById({ id: producto.id}).$promise
            //         .then(function() {
            //             $scope.productos.splice($index,1);
            //             $scope.loading=false;
            //         },function(msg){
            //             alert(msg);
            //             console.log(msg);
            //          });
            // };

            // $scope.update = function(producto){
            //     producto.$save();
            // };

            $scope.saveProduct = function(id){
                localStorageService.save(id);
                
                $scope.lsProductos = localStorageService.getAll(); // actualiza array de items
                $scope.cantidadItems = $scope.lsProductos.length; // actualiza cantidad
                $scope.productosSeleccionados = []; // limpio array para cargar productos de la base (api)
                $scope.getProducts(); // obtiene productos de la api
            }

        // Obtiene los productos de la base de datos que estan en local storage
            $scope.getProducts = function(){

                for (var i = 0; i < $scope.productos.length; i++) {
                    for (var j = 0; j < $scope.lsProductos.length; j++) {
                        if ($scope.productos[i].idproducto == $scope.lsProductos[j].idproducto) {
                            $scope.productos[i].seleccionado = true;
                            console.log($scope.productos[i]);
                        }
                    }
                }
                
                if ($scope.cantidadItems >= 1) {

                    var filter = {"filter": (
                                            { fields: {idproducto: true, scodproducto: true, snombre: true}}, 
                                            {"where":{"or": $scope.lsProductos}}
                    )};
                    // Trae los objetos, solo de los productos seleccionados, de la api.
                    productosApiService.getFiltered(filter).then(function (data){
                        $scope.productosSeleccionados = data; 
                        var filterPrecio = {"filter": (
                            {"where":{"or": $scope.lsProductos}, order: "fvigencia DESC"}
                        )};
                        // De los mismos productos traigo los precios
                        productosApiService.getPrice(filterPrecio).then(function (data){ 
                            for (var i = 0; i < $scope.productosSeleccionados.length; i++){ // Bucles para agregar precio y cantidad al objeto producto
                                for (var j = 0; j < data.length; j++){
                                    if (data[j].idproducto == $scope.productosSeleccionados[i].idproducto){
                                        $scope.productosSeleccionados[i].precio = data[j].rprecio; // Inserta un nuevo atributo al objeto preducto, precio
                                        // let cant = $filter('cantidad')($scope.productosSeleccionados[i]); 
                                        // Obtiene la cantidad seleccionada por el usuario mediante el filtro cantidad
                                        let cant =localStorageService.getCantidad($scope.productosSeleccionados[i].idproducto);
                                        $scope.productosSeleccionados[i].cantidad = cant; // Inserta atributo cantidad al objeto poducto
                                        $scope.sumaTotalSeleccionado += (cant * data[j].rprecio) // Realiza la suma del total de la compra
                                        break;
                                    }
                                }
                            }
                        });
                    });
                
                }
            }

            function recalculoTotal(){
                $scope.sumaTotalSeleccionado = 0;
                for (var i = 0; i < $scope.cantidadItems; i++){
                    $scope.sumaTotalSeleccionado += ($scope.productosSeleccionados[i].cantidad * $scope.productosSeleccionados[i].precio);
                }
            }

            $scope.lsDeleteProduct = function(id){
                localStorageService.delete(id);
            }

            $scope.viewDeleteProduct = function(i){
                $scope.productosSeleccionados.splice(i, 1);
                $scope.cantidadItems = $scope.productosSeleccionados.length;
                recalculoTotal();
            }

            //////////////////////////////////////////////////////////////
            /// FIN FUNCIONES Y METODOS
            ///////////////////////////////////////////////////////////// 

            //////////////////////////////////////////////////////////////
            /// MAIN
            ///////////////////////////////////////////////////////////// 

            // Obtengo todos los productos de la api para mostrar en la tabla
            var f = {
                "filter": (
                    { fields: { idproducto: true, scodproducto: true, snombre: true } }
                )
            };
            productosApiService.getFiltered(f).then(function (data) {
                $scope.productos = data;
                // Variables para resumen de carrito
                $scope.lsProductos = localStorageService.getAll();
                $scope.cantidadItems = $scope.lsProductos.length;
                $scope.totalSeleccionados = 0;
                $scope.getProducts();
            });

            //////////////////////////////////////////////////////////////
            /// FIN MAIN
            ///////////////////////////////////////////////////////////// 
        });
})();
